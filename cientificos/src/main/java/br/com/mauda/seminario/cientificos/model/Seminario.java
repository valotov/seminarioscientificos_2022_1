package br.com.mauda.seminario.cientificos.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Seminario {

    private List<Professor> professores = new ArrayList<>();
    private List<Inscricao> inscricoes = new ArrayList<>();
    private List<AreaCientifica> areasCientificas = new ArrayList<>();

    private Long id;
    private String titulo;
    private String descricao;
    private boolean mesaRedonda;
    private LocalDate data;// = LocalDate.now();
    private Integer qtdInscricoes;

    public Seminario(AreaCientifica area, Professor professor, Integer i) {
        this.qtdInscricoes = i;

        this.professores.add(professor);
        professor.adicionarSeminario(this);

        for (int j = 0; j < this.qtdInscricoes; j++) {
            new Inscricao(this);
        }
    }

    public void adicionarAreaCientifica(AreaCientifica area) {
        this.areasCientificas.add(area);
    }

    public void adicionarInscricao(Inscricao inscricao) {
        this.inscricoes.add(inscricao);
    }

    public void adicionarProfessor(Professor professor) {
        this.professores.add(professor);
    }

    public boolean possuiAreaCientifica(AreaCientifica area) {
        return this.areasCientificas.contains(area);
    }

    public boolean possuiProfessor(Professor professor) {
        return this.professores.contains(professor);
    }

    public List<Professor> getProfessores() {
        return this.professores;
    }

    public void setProfessores(List<Professor> professores) {
        this.professores = professores;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public void setInscricoes(List<Inscricao> inscricoes) {
        this.inscricoes = inscricoes;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.areasCientificas;
    }

    public void setAreasCientificas(List<AreaCientifica> areasCientificas) {
        this.areasCientificas = areasCientificas;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public boolean isMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public LocalDate getData() {
        return this.data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public Integer getQtdInscricoes() {
        return this.qtdInscricoes;
    }

    public void setQtdInscricoes(Integer qtdInscricoes) {
        this.qtdInscricoes = qtdInscricoes;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Seminario other = (Seminario) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

}
