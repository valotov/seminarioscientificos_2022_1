package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Boolean direitoMaterial;
    private SituacaoInscricaoEnum situacao;
    private Seminario seminario;
    private Estudante estudante;
    private LocalDateTime dataCriacao = LocalDateTime.now();
    private LocalDateTime dataCompra;
    private LocalDateTime dataCheckIn;

    public Inscricao(Seminario seminario) {
        super();
        this.seminario = seminario;
        this.seminario.getInscricoes().add(this);
        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
    }

    public LocalDateTime getDataCriacao() {
        return this.dataCriacao;
    }

    public void setDataCriacao(LocalDateTime dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public LocalDateTime getDataCompra() {
        return this.dataCompra;
    }

    public void setDataCompra(LocalDateTime dataCompra) {
        this.dataCompra = dataCompra;
    }

    public LocalDateTime getDataCheckIn() {
        return this.dataCheckIn;
    }

    public void setDataCheckIn(LocalDateTime dataCheckIn) {
        this.dataCheckIn = dataCheckIn;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public void setDireitoMaterial(Boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public void setSeminario(Seminario seminario) {
        this.seminario = seminario;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public void setEstudante(Estudante estudante) {
        this.estudante = estudante;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }

    public void setSituacao(SituacaoInscricaoEnum situacao) {
        this.situacao = situacao;
    }

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        estudante.adicionarInscricao(this);
        this.dataCompra = LocalDateTime.now();
        this.estudante = estudante;
        this.direitoMaterial = direitoMaterial;
        this.situacao = SituacaoInscricaoEnum.COMPRADO;
    }

    public void cancelarCompra() {
        this.estudante.removerInscricao(this);
        this.dataCompra = null;
        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
        this.estudante = null;
        this.direitoMaterial = null;
    }

    public void realizarCheckIn() {
        this.dataCheckIn = LocalDateTime.now();
        this.situacao = SituacaoInscricaoEnum.CHECKIN;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Inscricao other = (Inscricao) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

}
